import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
	<title> Ma page </title>
    <link id="css" rel="stylesheet" type="text/css" href="styles/style1.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Protest+Revolution&display=swap" rel="stylesheet">
</head>
<body>
   <nav role="navigation">
    <div id="menuToggle">
        <input type="checkbox" />
        <span></span>
        <span></span>
        <span></span>
        <ul id="menu">
            <a href="village.py"><li class="button2">Retourner au village</li></a>
            <a href="accueil.py"><li class="button2">Quitter la partie</li></a>
        </ul>
    </div>
  </nav>
    <img src="image/monstre1.png"></img>
    <form method="post" action="mission1_2.py">
    <p></br>
    <input type="submit" class="button1" value="attaquer"></p>
    </form>
</body>
</html>"""
print(html)