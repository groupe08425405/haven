import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
	<title> Ma page </title>
    <link id="css" rel="stylesheet" type="text/css" href="styles/style1.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Protest+Revolution&display=swap" rel="stylesheet">
</head>
<body>
	<form method="post" action="accueil.py">
    <p>Alors veux-tu rejoindre notre village et vivre cette incroyable aventure ?</br>
    <input type="submit" value="non"></p>
    </form>
    <form method="post" action="village.py">
    <input type="submit" value="oui">
    </form>
</body>
</html>"""
print(html)
